# Where I Live Page #

Using HTML to create a where I live page.

### Clone Repository ###

1. Fork this repository.
2. Git clone your own copy to your assignments folder.
3. Change directory into your local copy of the repo.

### To Do ###

1. Open where_i_live.html in Sublime and have a look at its structure.
2. You're going to create a page about where you live, feel free to copy and paste from the internet.
3. Add a title for your where I live page to the <head> of where_i_live.html.
4. Add an image to the <header> section, image styling is in style.css file.
5. Use both inline elements and block level elements to structure your page.
6. You can see your progress by typing in terminal 'open where_i_live.html'.